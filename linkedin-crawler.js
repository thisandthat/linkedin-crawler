// ==UserScript==
// @name         Linkedin crawler
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.linkedin.com/*
// @grant        unsafeWindow
// ==/UserScript==

function doScrape() {
    if (localStorage.getItem("liQ") === null) {
        console.log("no queue detected in localStorage, creating a new one");
        q = new Queue();
        localStorage.setItem("liQ", JSON.stringify(q));
    }
    console.log("retrieving queue from local storage");
    q = JSON.parse(localStorage.getItem("liQ"));
    q = reviver(q);
    if (localStorage.getItem("liVisited") === null) {
        console.log("no visited detected in localStorage, creating a new one");
        visited = {};
        localStorage.setItem("liVisited", JSON.stringify(visited));
    }
    visited = JSON.parse(localStorage.getItem("liVisited"));
    if ((matchWords(extractTitleFromProfilePage(), targetWords) > 1) && extractDegreeFromProfilePage() > 1) {
        console.log("matched 2 or more words, adding connection");
        addConnection();
    }
    parseSidebarNames(q);
}

function parseSidebarNames(q) {
    console.log("parsing the sidebar for names");
    elems = document.getElementsByClassName("pv-browsemap-section__member-container mt4");
    console.log("elems");
    console.log(elems);
    console.log(elems[0], elems[1]);
    for (let e of elems) {
        console.log("extracting info");
        info = extractInfo(e);
        console.log(info);
        // if (info.degree == 1) {
        //     continue
        // }
        if (visited[info.name]) {
            console.log("already visited this profile, continuing");
            continue;
        }
        if (matchWords(info.title, targetWords) > 0) {
            console.log("at least 1 word matched, adding name to queue");
            visited[info.name] = true;
            q.enqueue(info.name);
        }
    }
    console.log("current state of queue:");
    console.log(q);
    localStorage.setItem("liQ", JSON.stringify(q));
    localStorage.setItem("liVisited", JSON.stringify(visited));
    //wait to avoid bot detection, throttling, etc.
    //30 seconds plus a random delay from 1 to 100 seconds to make it look more believable
    randomDelay = getRandomInt(1000, 100000);
    setTimeout(function () {
        next = q.dequeue();
        if (next === undefined) {
            console.log("queue empty, returning");
            return;
        }
        localStorage.setItem("liQ", JSON.stringify(q));
        location.assign(liAddress+next);
    }, 30000+randomDelay);
}

function extractInfo(elem) {
    profileLink = elem.getElementsByTagName("a")[0].getAttribute("href");
    info = {};
    info.name = profileLink.slice(4,-1);
    info.title = elem.getElementsByTagName("p")[0].innerText;
    degree = elem.getElementsByClassName("visually-hidden")[0].innerText;
    info.degree = parseInt(degree.slice(0,1), 10);
    return info;
}

function extractTitleFromProfilePage() {
    title = document.getElementsByClassName("pv-top-card-section__headline")[0].innerText;
    if (title === undefined) {
        return null;
    }
    return title;
}

function extractDegreeFromProfilePage() {
    degree = document.getElementsByClassName("pv-top-card-section__body")[0].getElementsByClassName("visually-hidden")[0].innerText;
    if (degree === undefined) {
        return null;
    }
    return parseInt(degree.slice(0,1), 10);
}

function matchWords(title, words) {
    score = 0;
    title = title.toLowerCase();
    for (let word of words) {
        console.log("title is", title);
        console.log("word is", word);
        if (title.indexOf(word) != -1) {
            score += 1;
        }
    }
    return score;
}

function addConnection() {
    console.log("adding connection");
    connectButton = document.getElementsByClassName("connect primary top-card-action ember-view")[0];
    if (connectButton === undefined) {
        //try to find a connect button in the collapsible action menu
        //find the element of the collapsible menu
        collapsibleMenu = document.getElementsByClassName("pv-top-card-overflow__trigger button-tertiary-medium-round-inverse dropdown-trigger ember-view")[0];
        if (collapsibleMenu === undefined) {
            collapsibleMenu = document.getElementsByClassName("pv-top-card-overflow__trigger button-secondary-large-muted dropdown-trigger ember-view")[0];
        }
        if (collapsibleMenu === undefined) {
            console.log("can't find collapsible menu");
            return;
        }
        //open the collapsible menu
        collapsibleMenu.click();
        //get the connect button
        connectBox = document.getElementsByClassName("action connect overflow ember-view")[0];
        if (connectBox === undefined) {
            console.log("can't add this person");
            return;
        }
        connectButton = connectBox.getElementsByClassName("action-btn")[0];
        if (connectButton === undefined) {
            console.log("can't add this person");
            return;
        }
    }
    console.log(connectButton);
    connectButton.click();
    sendButton = document.getElementById("li-modal-container").getElementsByTagName("button")[2];
    if (connectButton === undefined) {
        console.log("can't send invite to connect");
        return;
    }
    console.log(sendButton);
    sendButton.click();
}

function reviver(o) {
    q = new Queue();
    q.front = o.front;
    q.rear = o.rear;
    q.size = o.size;
    q.queue = o.queue;
    return q;
}

//random int from MDN, available here: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

(function() {
    'use strict';

    // Your code here...
    console.log("defining constants");
    unsafeWindow.targetWords = ["technical", "tech", "IT", "engineering", "software", "recruiter", "recruitment", "sourcer", "sourcing", "headhunter", "talent", "acquisition", "google", "amazon", "facebook", "uber", "airbnb"];
    unsafeWindow.liAddress = "http://linkedin.com/in/";
    console.log("defining queue");

	//queue by Ankur Rastogi, available here: https://www.quora.com/Should-I-use-an-array-or-an-object-for-implementing-a-JavaScript-queue-class/answer/Ankur-Rastogi

	//Initialise Queue
    unsafeWindow.Queue = function () {
        this.front = 0;
        this.rear = 0;
        this.size = 0;
        this.queue = [];
    };

    //Enqueue - Add an item to queue
    Queue.prototype.enqueue = function (value) {
        this.queue[this.rear] = value;
        this.rear++;
        this.size++;
    };

    //Dequeue - Remove an item from the queue
    Queue.prototype.dequeue = function () {
        if (this.size === 0) return console.log("Nothing to dequeue, Queue is already empty.");
        var item = this.queue[this.front];
        this.front++;
        this.size--;
        return item;
    };

    //isEmpty - returns true if queue is empty
    Queue.prototype.isEmpty = function () {
        return (this.size === 0);
    };

    //Front - The pointer to the first inserted element
    Queue.prototype.front = function () {
        return this.front;
    };

    //Rear - The pointer to the last inserted element
    Queue.prototype.rear = function () {
        return this.rear;
    };

    //Size - returns the size of the queue at any point of time
    Queue.prototype.size = function () {
        return this.size;
    };

    unsafeWindow.testVar = "test variable";
    console.log(testVar);
    console.log("starting crawler");
    console.log("waiting for page load to finish");
    window.onload = setTimeout(function () {
        doScrape();
    }, 5000);

})();